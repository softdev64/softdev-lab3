/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package com.supanut.lab03;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author NutLekKung
 */
public class XOProgramUnitTest {

    public XOProgramUnitTest() {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
    }

    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void testCheckCrossWinPlayerX_1() {
        Lab03.board = new char[][]{{'X', '-', '-'},
        {'-', 'X', '-'},
        {'-', '-', 'X'},};
        assertEquals(true, Lab03.checkWinner());
    }

    @Test
    public void testCheckCrossWinPlayerX_2() {
        Lab03.board = new char[][]{{'-', '-', 'X'},
        {'-', 'X', '-'},
        {'X', '-', '-'},};
        assertEquals(true, Lab03.checkWinner());
    }

    @Test
    public void testCheckDrawPlayer() {
        Lab03.board = new char[][]{{'O', 'X', 'X'},
        {'X', 'X', 'O'},
        {'O', 'O', 'X'},};
        assertEquals(true, Lab03.checkDraw());
    }

    @Test
    public void testCheckColumnWinPlayerX_1() {
        Lab03.board = new char[][]{{'X', '-', '-'},
        {'X', '-', '-'},
        {'X', '-', '-'},};
        assertEquals(true, Lab03.checkWinner());
    }

    @Test
    public void testCheckColumnWinPlayerX_2() {
        Lab03.board = new char[][]{{'-', 'X', '-'},
        {'-', 'X', '-'},
        {'-', 'X', '-'},};
        assertEquals(true, Lab03.checkWinner());
    }

    @Test
    public void testCheckColumnWinPlayerX_3() {
        Lab03.board = new char[][]{{'-', '-', 'X'},
        {'-', '-', 'X'},
        {'-', '-', 'X'},};
        assertEquals(true, Lab03.checkWinner());
    }

    @Test
    public void testCheckRowWinPlayerX_1() {
        Lab03.board = new char[][]{{'X', 'X', 'X'},
        {'-', '-', '-'},
        {'-', '-', '-'},};
//        Lab03.player = 'X';
        assertEquals(true, Lab03.checkWinner());
    }
    
    @Test
    public void testCheckRowWinPlayerX_2() {
        Lab03.board = new char[][]{{'-', '-', '-'},
        {'X', 'X', 'X'},
        {'-', '-', '-'},};
//        Lab03.player = 'X';
        assertEquals(true, Lab03.checkWinner());
    }
    
    @Test
    public void testCheckRowWinPlayerX_3() {
        Lab03.board = new char[][]{{'-', '-', '-'},
        {'-', '-', '-'},
        {'X', 'X', 'X'},};
//        Lab03.player = 'X';
        assertEquals(true, Lab03.checkWinner());
    }
    
//     @Test
//    public void testCheckRowWinPlayerO_1() {
//        Lab03.board = new char[][]{{'-', '-', '-'},
//        {'-', '-', '-'},
//        {'O', 'O', 'O'},};
//        Lab03.player = 'O';
//        assertEquals(true, Lab03.checkWinner());
//    }
}
