package com.supanut.lab03;

import java.util.Scanner;

public class Lab03 {

    public static Scanner sc = new Scanner(System.in);
    public static char[][] board = {{'-', '-', '-'},
                                    {'-', '-', '-'},
                                    {'-', '-', '-'},};
    public static char player = 'X';
    public static int row, col;
//    public static int round = 0;
    public static String answer;
    
    public static void main(String[] args) {
        
        while (true) {
            printBoard(board);
            printTurn(player);
            inputRowCol();

            if (checkWinner()) {
                printBoard(board);
                printWinner(player);
                if(wantPlayagin()){
                    resetBoard(board);
//                    resetRound(round);
                    continue;
                } 
                break;
            }
            if (checkDraw()){
                printBoard(board);
                printDraw();
                if(wantPlayagin()){
                    resetBoard(board);
//                    resetRound(round);
                    continue;
                } 
                break;
            }

//            countRound();
            switchPlayer();
        }
    }
    
    private static void printBoard(char[][] board) {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(board[i][j] + " ");
            }
            System.out.println("");
        }
    }
    
    private static void resetBoard(char[][] board) {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                board[i][j] = '-';
            }
        }
    }
    
//    private static void resetRound(int Round) {
//        round = 0;
//    }

    private static void printTurn(char player) {
        System.out.println(player + " : turn");
    }
    
     private static void printDraw() {
        System.out.println("Draw!!!");
    }
    
    static boolean checkDraw() {
        for (int row = 0; row < 3; row++) {
        for (int col = 0; col < 3; col++) {
            if (board[row][col] != 'X' && board[row][col] != 'O') {
                return false;
            }
        }
    }
    return true;
//        if(round == 8){
//            return true;
//        }
//        return false;
    }

    private static void inputRowCol() {
        System.out.print("Please input row,col: ");
        row = sc.nextInt();
        col = sc.nextInt();

        while(board[row - 1][col - 1] != '-'){
            System.out.println("Please input row,col agin:");
            row = sc.nextInt();
            col = sc.nextInt();
        }

        board[row - 1][col - 1] = player;
    }

//    private static void countRound() {
//        round += 1;
//    }
    
    private static boolean wantPlayagin() {
        System.out.println("Play again? (y/n)");
        answer = sc.next();
        if(answer.equals("y")){
            return true;
        }else if(answer.equals("n")){
            return false;
        }
        return false;
    }

    private static void switchPlayer() {
        if (player == 'X') {
            player = 'O';
        } else if (player == 'O') {
            player = 'X';
        }
    }

    static boolean checkWinner() {
        if (checkRow(board) || checkCol(board) || checkCross(board)) {
            return true;
        }
        return false;
    }

     static boolean checkRow(char[][] board) {
        for (int row = 0; row < 3; row++) {
            if (board[row][0] == player && board[row][1] == player && board[row][2] == player) {
                return true;
            }
        }
        return false;

    }

     static boolean checkCol(char[][] board) {
        for (int col = 0; col < 3; col++) {
            if (board[0][col] == player && board[1][col] == player && board[2][col] == player) {
                return true;
            }
        }
        return false;
    }

     static boolean checkCross(char[][] board) {
        if (board[0][0] == player && board[1][1] == player && board[2][2] == player) {
            return true;
        }

        else if (board[0][2] == player && board[1][1] == player && board[2][0] == player) {
            return true;
        }
        return false;

    }

    private static void printWinner(char player) {
        System.out.println(player+" : WIN!!!");
    }

}
